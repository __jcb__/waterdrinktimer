import {useState, useEffect} from "react";
import Timer from "./Timer";
import TimeBeforeDisplay from "./TimeBeforeDisplay";
import ModeDisplay from "./ModeDisplay";



function App_() {
  const [lastTime, setLastTime] = useState(0);
  const [mode, setMode] = useState("startScreen");
  const [color, setColor] = useState("#EE6644");
    const [releaseFlag, setReleaseFlag] = useState(true);

    const clickHandler = (clickType) => {
      if(mode === "startScreen" && clickType === "click" && releaseFlag) {
          setMode("ready")
          setColor("#ffb344")
      }
      if(mode === "ready" && clickType === "release") {
          setMode("drink")
          setColor("#77ff88")
      }
      if(mode === "drink" && clickType === "click"){
          setMode("startScreen")
          setColor("#EE6644")
          setReleaseFlag(false)

      }
      if(clickType === "release"){
          setReleaseFlag(true)
      }
  }


    useEffect(() => {
        return () => {
            console.log(mode, color)
        };
    }, [mode]);



  return (
    <div className="App_"
         onMouseDown={() => {clickHandler("click")}}
         onTouchStartCapture={() => {clickHandler("click")}}
         onMouseUp={() => {clickHandler("release")}}
         onClick={() => {clickHandler("release")}}
         style={{backgroundColor: color, height: '100%', position: 'absolute',
             left: '0px', width: '100%', overflow: 'hidden', color: "#723040", padding: "10%",
             userSelect: "none", msTouchSelect: "none"}}>
        <div>
      <ModeDisplay mode={mode}/>
      <Timer mode={mode} setLastTime={setLastTime}/>
      <TimeBeforeDisplay lastTime={lastTime}/>
        </div><div style={{height: "auto",  width: '100%'}}></div>

    </div>
  );
}

export default App_;
