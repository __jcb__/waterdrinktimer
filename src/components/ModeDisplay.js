import React from 'react';

function ModeDisplay(props) {
    const xPlain = {"startScreen": "Place a filled on this phone and hold down a finger.", "drink": "Drink the cup. Then touch this phone.", "ready": "Start the timer by lifting your finger and the cup."}
    return (
        <>
            <h1 id={"modeDisplay"}>{{"startScreen": "Stop", "ready": "Get Ready...", "drink": "Drink!!!"}[props.mode]}</h1>
            <h3>{xPlain[props.mode]}</h3>
        </>
    );
}

export default ModeDisplay;