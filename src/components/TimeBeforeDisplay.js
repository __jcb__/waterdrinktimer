import React from 'react';

function TimeBeforeDisplay(props) {
    return (
        <h2 id={"timeDisplay"}>You needed <b>{Math.floor(props.lastTime/1000)},{props.lastTime % 1000}</b>s</h2>
    );
}

export default TimeBeforeDisplay;