import React, {useEffect, useState} from 'react';

function Timer(props) {
    const date = new Date()
    const [beginTime, setBeginTime] = useState(0);


    useEffect(() => {
        if(props.mode === "drink") {
            setBeginTime(date.getTime())
        }
        if(props.mode === "startScreen"){
            props.setLastTime(date.getTime() - beginTime)

        }

    }, [props.mode])

    return (
        <div></div>
    );
}

export default Timer;